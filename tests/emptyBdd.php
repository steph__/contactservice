<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require_once __DIR__.'/../src/ContactService.php';

$contacts = new ContactService();
$contacts->init('contactsTest.sqlite');
$contacts->deleteAllContact();
